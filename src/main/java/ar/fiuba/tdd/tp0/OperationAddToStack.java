package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class OperationAddToStack implements IOperation {

	private Stack<Float> numbers;

	public OperationAddToStack(Stack<Float> numbers) {
		this.numbers=numbers;
	}

	@Override
	public void execute(String param){
		numbers.add(Float.parseFloat(param));
	}

}
