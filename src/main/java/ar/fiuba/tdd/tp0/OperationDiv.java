package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class OperationDiv implements IOperation{

	private Stack<Float> numbers;

	public OperationDiv(Stack<Float> numbers) {
		this.numbers=numbers;}


	@Override
	public void execute(String param) {
		float x=numbers.pop();
		float y=numbers.pop();
		numbers.push(y/x);
	}

}
