package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class OperationDivAll implements IOperation{

	private Stack<Float> numbers;

	public OperationDivAll(Stack<Float> numbers) {
		this.numbers=numbers;}


	@Override
	public void execute(String param) {
		while(numbers.size()>=2){
			float x=numbers.pop();
			float y=numbers.pop();
			numbers.push(x/y);
		}

	}
}
