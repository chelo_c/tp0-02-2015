package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class Helper {

	private  IValidator validSingle=null;
	private  IValidator validMulti=null;



	Helper(Stack<Float> numbers){

		validMulti= new IValidator() {
			@Override
			public void validate() {
				System.out.println("numbers:"+numbers.size());
				if(numbers.size()<1)throw new IllegalArgumentException();
			}
		};

		validSingle= new IValidator() {
			@Override
			public void validate() {
				System.out.println("numbers:"+numbers.size());
				if(numbers.size()<2)throw new IllegalArgumentException();
			}
		};

	}


	public IValidator getValidMulti() {
		return validMulti;
	}


	public  IValidator getValidSingle() {
		return validSingle;
	}
}
