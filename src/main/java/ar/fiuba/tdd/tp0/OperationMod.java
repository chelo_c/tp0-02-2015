package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class OperationMod implements IOperation{

	private Stack<Float> numbers;

	public OperationMod(Stack<Float> numbers) {
		this.numbers=numbers;}


	@Override
	public void execute(String param) {
		float x=numbers.pop();
		float y=numbers.pop();
		numbers.push(y % x);
	}

}
