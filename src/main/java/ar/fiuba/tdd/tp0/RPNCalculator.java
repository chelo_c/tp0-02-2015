package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Stack;

import com.sun.istack.internal.NotNull;

public class RPNCalculator {
	
	private HashMap<String, IOperation> operations;
	private OperationAddToStack defaultOp;
	private Stack<Float> numbers;
	private HashMap<String, IValidator> validator;
	private IValidator defaultValid;

	public RPNCalculator(){
		
		numbers = new Stack<Float>();
		
		operations= new HashMap<String, IOperation>();
		operations.put("+", new OperationSum(numbers));
		operations.put("-", new OperationSub(numbers));
		operations.put("*", new OperationMult(numbers));
		operations.put("/", new OperationDiv(numbers));
		operations.put("MOD", new OperationMod(numbers));
		

		operations.put("++", new OperationSumAll(numbers));
		operations.put("--", new OperationSubAll(numbers));
		operations.put("**", new OperationMultall(numbers));
		operations.put("//", new OperationDivAll(numbers));
		operations.put("MODMOD", new OperationModAll(numbers));
		
		
		Helper help= new Helper(numbers);
		validator= new HashMap<String, IValidator>();
		validator.put("+", help.getValidSingle());
		validator.put("-", help.getValidSingle());
		validator.put("*", help.getValidSingle());
		validator.put("/", help.getValidSingle());
		validator.put("MOD", help.getValidSingle());
		
		validator.put("++", help.getValidMulti());
		validator.put("--", help.getValidMulti());
		validator.put("**", help.getValidMulti());
		validator.put("//", help.getValidMulti());
		validator.put("MODMOD", help.getValidMulti());
		
		
		defaultValid = new IValidator() {
			@Override
			public void validate() {
			}
		};
		defaultOp = new OperationAddToStack(numbers);
		
	}

    public float eval(String expression) {
    	if(expression==null)throw new IllegalArgumentException();
    	//String expression = new String(s);
    	String[] parameters = expression.split(" ");
    	//float result = 0;
    	for (int i = 0; i < parameters.length; i++) {
			String param = parameters[i];
			System.out.println("parameter: "+param);
			
			
			IValidator val=validator.getOrDefault(param,defaultValid);
			val.validate();
			
			IOperation op=operations.getOrDefault(param,defaultOp);
			op.execute(param);
			System.out.println(numbers);
			
		}
    	return numbers.firstElement();
    	
    }

}
